# Redis Cluster CLient

## Using

The main branch only contains this README.
There are branches for all of the examples

## Go

### golang branch

Use go-redis driver to connect to Memorystore Flex

## Python

### python branch

Use python driver to connect to Memorystore Flex


## Java 

### lettuce branch

This shows how to connect to Memorystore Flex with Lettuce

### jedis branch

This shows how to connect to Memorystore Flex with Jedis


